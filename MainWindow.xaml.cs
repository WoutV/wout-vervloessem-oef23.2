﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vervloessem_Wout_oef23._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
  
 

        private void btnBereken_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtStraal.Text) && txtStraal.Text.All(char.IsDigit))
            {
                double d = Convert.ToDouble(txtStraal.Text);
                tbOmtrek.Text = Math.Round((2 * Math.PI * d), 2).ToString();
                tbOppervlakte.Text = Math.Round((Math.PI * Math.Pow(d, 2)), 2).ToString();
            }
            else
            {
                MessageBox.Show("Gelieve een getal in te geven");
                txtStraal.Clear();
                txtStraal.Focus();
                
            }
        }

    }
}
